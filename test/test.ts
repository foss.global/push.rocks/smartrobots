import { expect, tap } from '@pushrocks/tapbundle';
import * as smartrobots from '../ts/index';

let testSmartrobotsInstance: smartrobots.Smartrobots;

tap.test('first test', async () => {
  testSmartrobotsInstance = new smartrobots.Smartrobots();
  expect(testSmartrobotsInstance).to.be.instanceOf(smartrobots.Smartrobots);
});

tap.test('should parse a robots.txt', async () => {
  const result = await testSmartrobotsInstance.parseRobotsTxtFromUrl('https://central.eu/robots.txt');
  console.log(result);
})

tap.start();
